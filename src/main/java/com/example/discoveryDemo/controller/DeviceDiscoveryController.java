package com.example.discoveryDemo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.discoveryDemo.model.Device;
import com.example.discoveryDemo.service.SNMPDiscoveryService;

@RestController
public class DeviceDiscoveryController {
	String[] ipArray = {"192.168.5.1","192.168.5.41"};
	List<String> ipList = Arrays.asList(ipArray);
	
	@Autowired
	SNMPDiscoveryService discoveryService;
	
	@GetMapping("/devices")
	List<Device> getAllDevices(){
		
	
		return discoveryService.discvoverDevices(ipList);
		
	}

}
