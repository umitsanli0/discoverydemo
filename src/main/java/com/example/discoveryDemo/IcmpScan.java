package com.example.discoveryDemo;


import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;



public class IcmpScan{
	
	private  boolean isReachable(String ip) {
		try {
			if(InetAddress.getByName(ip).isReachable(250)) {     
				return true; 
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return false;
	}
	
	public List<String> scan(String subIp){
		List<String> ipList = new ArrayList<>();
		List<String> reachable = new ArrayList<>();
		
//		subIp = "192.168.5.%d";
		for(int i=1; i<255;i++) {
			//for(int j=1; j< 255; j++) {
				//ipList.add(String.format(subIp,i ));
				ipList.add(subIp+ i);
			//}
		}
		
		ipList.stream()
			.parallel()
			.filter(this::isReachable)
			.collect(Collectors.toList())
			.forEach(reachable::add);
		
		
		System.out.println("After ICMP Scanning...");
		System.out.println("Reachable IP Addresses :");
		for(String ips : reachable)
			System.out.println(ips);
		
		return reachable;
	}
}
