package com.example.discoveryDemo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeListener;
import org.snmp4j.util.TreeUtils;
import org.springframework.stereotype.Service;
import com.example.discoveryDemo.IcmpScan;
import com.example.discoveryDemo.model.Device;
import com.example.discoveryDemo.model.Port;

@Service
public class SNMPDiscoveryService implements ResponseListener, TreeListener{

	// MIB LISTS
	private static final String OID_SYS_NAME = "1.3.6.1.2.1.1.5.0";
	private static final String OID_MAC_ADDRESS = "1.3.6.1.2.1.2.2.1.6.2";
	private static final String OID_SYS_STATUS = "1.3.6.1.2.1.2.2.1.7.1";
	private static final String OID_SYS_UPTIME = "1.3.6.1.2.1.1.3.0";
	private static final String OID_SYS_OPSYS = "1.3.6.1.2.1.1.1.0";
	private static final String OID_DEVICE_ROLE = "1.3.6.1.2.1.1.5.0";
	private static final String OID_SERIAL_NUMBER = "1.3.6.1.2.1.47.1.1.1.1.11";
	private static final String OID_VENDOR = "	1.3.6.1.2.1.47.1.1.1.1.3";
	private static final String OID_LOCATION = "1.3.6.1.2.1.1.6.0";

	private static final String OID_PORT_NAME = "1.3.6.1.2.1.2.2.1.2";
	private static final String OID_PORT_MODE = "1.3.6.1.2.1.2.2.1.7";
	private static final String OID_PORT_STATE = "1.3.6.1.2.1.2.2.1.8";
	private static final String OID_PORT_SPEED = "1.3.6.1.2.1.2.2.1.5";
	private static final String OID_PORT_MTU = "1.3.6.1.2.1.2.2.1.4";
	
	private static String[] deviceOIDs =  { OID_SYS_NAME, OID_MAC_ADDRESS, OID_SYS_STATUS, OID_SYS_UPTIME, OID_SYS_OPSYS, OID_VENDOR,OID_LOCATION };
	private static String[] portOids =  {OID_PORT_NAME, OID_PORT_STATE, OID_PORT_SPEED, OID_PORT_MTU};

	private static int snmpVersion = SnmpConstants.version2c;
	private static final String comStr = "public";
	private static final int SNMP_DEFAULT_PORT = 161;
	
	private static int retries = 1;
	private static int timeout = 3000;
	
	private Port portn;
	private List<Port> portListn;
	
	
	// SNMP GET 
	public void snmpgetBulk(String ipAddr, int port, String[] oid) {
		try {
			TransportMapping<?> transport = new DefaultUdpTransportMapping();
			transport.listen();

			CommunityTarget comtarget = createTarget(comStr, ipAddr, port, snmpVersion);

			PDU pdu = new PDU();
			pdu.addAll(this.createOIDList(oid));
			pdu.setType(PDU.GET);
			pdu.setMaxRepetitions(1);
			pdu.setNonRepeaters(0);

			Snmp snmp = new Snmp(transport);

			System.out.println("Sending Request to Agent : " + ipAddr);
			
		    snmp.get(pdu, comtarget, null, this);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	//SNMP ON RESPONSE
	@Override
	public void onResponse(ResponseEvent event) {
		if(event.getResponse() != null) {
			if(event.getResponse().getErrorStatus() == PDU.noError) {
				((Snmp) event.getSource()).cancel(event.getRequest(), this);
				
				System.out.println("SNMP RESPONSE : " + event.getResponse());
				Map<String,String> resultMap = objectArrayToMap(event.getResponse().getVariableBindings().toArray());
				String ip = event.getPeerAddress().toString().split("/")[0];
				
				Device device =  Device.deviceBuilder(ip)
						.name(resultMap.get(OID_SYS_NAME))
						.mac(resultMap.get(OID_MAC_ADDRESS))
						.status(resultMap.get(OID_SYS_STATUS))
						.upTime(resultMap.get(OID_SYS_UPTIME))
						.opSystem(resultMap.get(OID_SYS_OPSYS))
						.vendor(resultMap.get(OID_VENDOR))
						.location(resultMap.get(OID_LOCATION))
						.port(setPortAttributes(ip))
						.build();
				
				saveDevice(device);
			}
			else {
				System.out.println("SNMP RESPONSE : " + event.getResponse());
			}
		}
		
	}

	// SNMP WALK 
	public Map<String, String> snmpwalkBulk(String ipAddr, int snmpport, OID[] oids) throws Exception {

		Map<String, String> hashMap = new HashMap<String, String>();
		TransportMapping<? extends Address> transport = new DefaultUdpTransportMapping();
		Snmp snmp = new Snmp(transport);
		transport.listen();

		// Setting Up Target
		CommunityTarget commTarget = createTarget(comStr, ipAddr, snmpport, snmpVersion);
		
		try {
			TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
			treeUtils.setMaxRepetitions(100);
			treeUtils.walk(commTarget, oids, null, this);
			//OLD CODE
//			TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
//			treeUtils.setMaxRepetitions(100);
//			List<TreeEvent> events = treeUtils.walk(commTarget, oids);
//			if (events == null || events.size() == 0)
//				System.out.println("No result returned");
//
//			for (TreeEvent event : events) {
//				if (event == null)
//					continue;
//				if (event.isError()) {
//					System.err.println("oid [" + oids + "] " + event.getErrorMessage());
//					continue;
//				}
//				VariableBinding[] varBindings = event.getVariableBindings();
//
//				if (varBindings == null || varBindings.length == 0)
//					continue;
//				for (VariableBinding varBinding : varBindings) {
//					if (varBinding == null)
//						continue;
//					
//					hashMap.put(varBinding.getOid().toString(), varBinding.getVariable().toString());
//				}
//			}
//			snmp.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return hashMap;
	}
	
	private CommunityTarget createTarget(String community, String ipAddr, int port, int version) {
		CommunityTarget target = new CommunityTarget();
		target.setCommunity(new OctetString(community));
		target.setAddress(new UdpAddress(ipAddr + "/" + port));
		target.setVersion(version);
		target.setRetries(retries);
		target.setTimeout(timeout);
		return target;
	}

	private Device discover(String ipAddr) {
	
        snmpgetBulk(ipAddr, SNMP_DEFAULT_PORT, deviceOIDs);

		return null;
	}
	
	public List<Device> discvoverDevices(List<String> ipList) {

		return ipList.stream()
				//.parallel()
				.map(this::discover)
				.collect(Collectors.toList());
	}

	private List<Port> setPortAttributes(String ipAddr) {
		String name;
		int state, speed, mtu;
		
		OID[] oid = createOIDArray(portOids);
	
		try {
			
			List<Port> portList = new ArrayList<>();
			Map<String, String> portMap = this.snmpwalkBulk(ipAddr, SNMP_DEFAULT_PORT, oid);
			List<String> keyList = new ArrayList<String>(portMap.keySet());
			
			Map<Object, List<String>> portGroup = keyList.stream()
			.collect(Collectors.groupingBy(k->k.substring(k.lastIndexOf(".")+1,k.length())));

			for(Map.Entry<Object, List<String>> entry : portGroup.entrySet()) {
				name = portMap.get(OID_PORT_NAME + "."+ entry.getKey());
				state = Integer.parseInt(portMap.get(OID_PORT_STATE + "."+ entry.getKey()));
				speed = Integer.parseInt(portMap.get(OID_PORT_SPEED + "."+ entry.getKey()));
				mtu = Integer.parseInt(portMap.get(OID_PORT_MTU + "."+ entry.getKey()));
				
				if(!portMap.get(OID_PORT_NAME + "." + entry.getKey()).equals("lo")) {
					Port port = Port.portBuilder(name)
								.state(state)
								.speed(speed)
								.mtu(mtu)
								.build();
					
					portList.add(port);
				}
			}
			
			return portList;
			
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}

	private List<VariableBinding> createOIDList(String[] oids) {
		List<VariableBinding> oidList = new ArrayList<>();

		for (String oid : oids)
			oidList.add(new VariableBinding(new OID(oid)));

		return oidList;
	}
	
	private OID[] createOIDArray(String[] oids) {
		OID[] oidArray = new OID[oids.length];
		for(int i = 0; i< oids.length; i++) {
			OID oid = new OID(oids[i]);
			oidArray[i] = oid;
		}
		return oidArray;
	}
	
	private Map<String, String> objectArrayToMap(Object[] objects) {
	    return Arrays.stream(objects)
	            .map(Object::toString)
	            .map(s->s.split("="))
	            .collect(Collectors.toMap(a->a[0].trim(),a->a[1].trim()));
	}
	
	private void saveDevice(Device device) {
		System.out.println("DEVICE : " + device);
	}

	@Override
	public boolean next(TreeEvent event) {
		VariableBinding[] varBindings = event.getVariableBindings();
		String oid;
		
		for (VariableBinding varBinding : varBindings) {
			if(varBinding != null) {
				oid = varBinding.getOid().toString();
				oid = oid.substring(0,oid.lastIndexOf("."));
				System.out.println(varBinding.getOid() + " ---> " + varBinding.getVariable() + " ~~~~~~~ OID : " + oid);
				
			}
		}
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	
		return false;
	}

	@Override
	public void finished(TreeEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

}
