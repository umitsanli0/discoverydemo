package com.example.discoveryDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscoveryDemoApplication {

	

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(DiscoveryDemoApplication.class, args);
	}
	
	

}
