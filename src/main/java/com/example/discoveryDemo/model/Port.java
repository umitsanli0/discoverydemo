package com.example.discoveryDemo.model;


public class Port{
	


	private String name;
	private int state;
	private int speed;
	private int mtu;
	
	private Port(PortBuilder portBuilder) {
		this.name = portBuilder.name;
		this.state = portBuilder.state;
		this.speed = portBuilder.speed;
		this.mtu = portBuilder.mtu;
	}
	
	public String getName() {
		return name;
	}
	public int getState() {
		return state;
	}
	public int getSpeed() {
		return speed;
	}
	public int getMtu() {
		return mtu;
	}
	
	public static PortBuilder portBuilder(String name) {
		return new Port.PortBuilder(name);
	}
	
	@Override
	public String toString() {
		return "Port [name=" + name + ", state=" + state + ", speed=" + speed + ", mtu=" + mtu + "]";
	}

	//Builder Class
    public static class PortBuilder{
    	private String name;
    	private int state;
    	private int speed;
    	private int mtu;
    	
    	public PortBuilder(String name) {
    		this.name = name;
    	}
    	
    	public PortBuilder state(int state) {
    		this.state = state;
    		return this;
    	}
    	
    	public PortBuilder speed(int speed) {
    		this.speed = speed;
    		return this;
    	}
    	
    	public PortBuilder mtu(int mtu) {
    		this.mtu = mtu;
    		return this;
    	}
    	
    	public Port build() {
    		Port port = new Port(this);
    		return port;
    	}
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
    public boolean equals(Object o) {
 	   if (o == this) {
            return true;
        }
        if (!(o instanceof Port)) {
            return false;
        }
        final Port other = (Port) o;
        return this.name.equals(other.name);
    }
}
