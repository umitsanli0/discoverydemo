package com.example.discoveryDemo.model;

import java.util.List;


public class Device {

	private String deviceName;
	private String macAddr;
	private String ipAddr;
	private String status;
	private String upTime;
	private String opSystem;
	private String deviceRole;
	private String serialNo;
	private String location;
	private String vendor;
	private List<Port> port;

	private Device(DeviceBuilder deviceBuilder) {
		this.deviceName =deviceBuilder.deviceName;
		this.macAddr = deviceBuilder.macAddr;
		this.ipAddr = deviceBuilder.ipAddr;
		this.status = deviceBuilder.status;
		this.upTime = deviceBuilder.upTime;
		this.opSystem = deviceBuilder.opSystem;
		this.deviceRole = deviceBuilder.deviceRole;
		this.serialNo = deviceBuilder.serialNo;
		this.location = deviceBuilder.location;
		this.vendor = deviceBuilder.vendor;
		this.port = deviceBuilder.port;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getMacAddr() {
		return macAddr;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public String getStatus() {
		return status;
	}

	public String getUpTime() {
		return upTime;
	}
	public String getOpSystem() {
		return opSystem;
	}

	public String getDeviceRole() {
		return deviceRole;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public String getLocation() {
		return location;
	}

	public String getVendor() {
		return vendor;
	}

	public List<Port> getPort() {
		return port;
	}
	
	public static  DeviceBuilder deviceBuilder(String ipAddr) {
		return new Device.DeviceBuilder(ipAddr);
	}
	
	//Builder Class
	public static class DeviceBuilder{
		private String deviceName;
		private String macAddr;
		private String ipAddr;
		private String status;
		private String upTime;
		private String opSystem;
		private String deviceRole;
		private String serialNo;
		private String location;
		private String vendor;
		private List<Port> port;
		
		public DeviceBuilder(String ipAddr) {
			this.ipAddr = ipAddr;
		}
		public DeviceBuilder name(String deviceName) {
			this.deviceName = deviceName;
			return this;
		}
		public DeviceBuilder mac(String macAddr) {
			this.macAddr = macAddr;
			return this;
		}
		public DeviceBuilder status(String status) {
			this.status = status;
			return this;
		}
		public DeviceBuilder upTime(String upTime) {
			this.upTime = upTime;
			return this;
		}
		public DeviceBuilder opSystem(String opSystem) {
			this.opSystem = opSystem;
			return this;
		}
		public DeviceBuilder deviceRole(String deviceRole) {
			this.deviceRole = deviceRole;
			return this;
		}
		public DeviceBuilder serialNo(String serialNo) {
			this.serialNo = serialNo;
			return this;
		}
		public DeviceBuilder location(String location) {
			this.location = location;
			return this;
		}
		public DeviceBuilder vendor(String vendor) {
			this.vendor = vendor;
			return this;
		}
		public DeviceBuilder port(List<Port> port) {
			this.port = port;
			return this;
		}
		public Device build() {
			Device device = new Device(this);
			return device;
		}
	}

	@Override
	public String toString() {
		return "Device [deviceName=" + deviceName + ", macAddr=" + macAddr + ", ipAddr=" + ipAddr + ", status=" + status
				+ ", upTime=" + upTime + ", opSystem=" + opSystem + ", deviceRole=" + deviceRole + ", serialNo="
				+ serialNo + ", location=" + location + ", vendor=" + vendor + ", port=" + port + "]";
	}
	
	

}
